From: panpanpanpan <panjeoc@proton.me>
Date: Wed, 21 Aug 2024 19:35:17 -0500
Subject: arm64: dts: sdm670-google-common: add IMX363 rear camera

The Sony IMX363 is the rear camera on the Pixel 3a/3a XL. It is
connected to cci_i2c0 and utilizes MCLK0 clocks. This commit adds
support for it in the Device Tree by including the necessary supplies
(vif, vaa, vdig, and vio) as referenced in the downstream Device Tree:
https://github.com/LineageOS/android_kernel_google_msm-4.9/blob/54588e2/arch/arm64/boot/dts/google/sdm670-camera-sensor-v2.dtsi

Signed-off-by: panpanpanpan <panjeoc@proton.me>
[richard: add newlines to separate first line, body, and tags; remove parent tag]
Signed-off-by: Richard Acayan <mailingradian@gmail.com>
---
 arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi | 90 +++++++++++++++++++++-
 1 file changed, 88 insertions(+), 2 deletions(-)

diff --git a/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi b/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
index e71ea9a..d56665f 100644
--- a/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
+++ b/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
@@ -416,6 +416,20 @@ cam_front_ldo: cam-front-ldo-regulator {
 		pinctrl-0 = <&cam_front_ldo_pin>;
 	};
 
+	cam_rear_ldo: cam-rear-ldo-regulator {
+		compatible = "regulator-fixed";
+		regulator-name = "cam_rear_ldo";
+		regulator-min-microvolt = <1352000>;
+		regulator-max-microvolt = <1352000>;
+		regulator-enable-ramp-delay = <233>;
+
+		gpios = <&pm660l_gpios 3 GPIO_ACTIVE_HIGH>;
+		enable-active-high;
+
+		pinctrl-names = "default";
+		pinctrl-0 = <&cam_rear_ldo_pin>;
+	};
+
 	cam_vio_ldo: cam-vio-ldo-regulator {
 		compatible = "regulator-fixed";
 		regulator-name = "cam_vio_ldo";
@@ -659,6 +673,14 @@ &camss {
 	status = "okay";
 };
 
+&camss_port0 {
+	camss_endpoint0: endpoint {
+		clock-lanes = <7>;
+		data-lanes = <0 1 2 3>;
+		remote-endpoint = <&cam_rear_endpoint>;
+	};
+};
+
 &camss_port1 {
 	camss_endpoint1: endpoint {
 		clock-lanes = <7>;
@@ -669,12 +691,48 @@ camss_endpoint1: endpoint {
 
 &cci {
 	pinctrl-names = "default", "sleep";
-	pinctrl-0 = <&cci1_default &cam_mclk_default>;
-	pinctrl-1 = <&cci1_sleep>;
+	pinctrl-0 = <&cci0_default &cci1_default &cam_mclk_default>;
+	pinctrl-1 = <&cci0_sleep &cci1_sleep>;
 
 	status = "okay";
 };
 
+&cci_i2c0 {
+	camera@1a {
+		compatible = "sony,imx363";
+		reg = <0x1a>;
+
+		clocks = <&camcc CAM_CC_MCLK0_CLK>;
+		clock-names = "xvclk";
+
+		clock-frequency = <24000000>;
+
+		assigned-clocks = <&camcc CAM_CC_MCLK0_CLK>;
+		assigned-clock-rates = <24000000>;
+
+		reset-gpios = <&tlmm 127 GPIO_ACTIVE_HIGH>;
+
+		vif-supply = <&cam_vio_ldo>;
+		vana-supply = <&cam_rear_ldo>;
+		vdig-supply = <&cam_rear_ldo>;
+		vio-supply = <&cam_vio_ldo>;
+
+		pinctrl-names = "default";
+		pinctrl-0 = <&cam_rear_default>;
+
+		rotation = <90>;
+		orientation = <1>;
+
+		port {
+			cam_rear_endpoint: endpoint {
+				data-lanes = <0 1 2 3>;
+				link-frequencies = /bits/ 64 <321000000 636000000>;
+				remote-endpoint = <&camss_endpoint0>;
+			};
+		};
+	};
+};
+
 &cci_i2c1 {
 	camera@1a {
 		compatible = "sony,imx355";
@@ -1035,6 +1093,13 @@ led-0 {
 };
 
 &pm660l_gpios {
+	cam_rear_ldo_pin: cam-rear-state {
+		pins = "gpio3";
+		function = "normal";
+		power-source = <PM8941_GPIO_VPH>;
+		output-low;
+	};
+
 	cam_front_ldo_pin: cam-front-state {
 		pins = "gpio4";
 		function = "normal";
@@ -1159,6 +1224,20 @@ rt5514_spi: audio-codec@0 {
 &tlmm {
 	gpio-reserved-ranges = <0 4>, <81 4>;
 
+	cam_rear_default: cam-rear-default-state {
+		vana-pins {
+			pins = "gpio8";
+			output-high;
+		};
+
+		reset-pins {
+			pins = "gpio127";
+			function = "gpio";
+			drive-strength = <2>;
+			bias-disable;
+		};
+	};
+
 	cam_front_default: cam-front-default-state {
 		pins = "gpio9";
 		function = "gpio";
@@ -1167,6 +1246,13 @@ cam_front_default: cam-front-default-state {
 	};
 
 	cam_mclk_default: cam-default-state {
+		mclk0-pins {
+			pins = "gpio13";
+			function = "cam_mclk";
+			drive-strength = <2>;
+			bias-disable;
+		};
+
 		mclk2-pins {
 			pins = "gpio15";
 			function = "cam_mclk";
