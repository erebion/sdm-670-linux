From: Richard Acayan <mailingradian@gmail.com>
Date: Mon, 11 Sep 2023 18:08:58 -0400
Subject: ASoC: qdsp6: q6voice-dai: reserve bit in shift attribute for rx/tx

The SDM670 and SDM845 kernel forks need to support the VoiceMMode1 path
type/voice mixer and use the shift register to support both VoiceMMode1
and CS-Voice. This creates a conflict when using the MSM8916 version of
the mixer controls patch, as the SDM670 kernel does.

Originally in SDM670, the logic was duplicated for both RX and TX, and
different functions have been referenced. This is not a great solution,
since the logic is almost the same except for a boolean passed to the
other functions.

Another idea was to indicate the path as RX/TX in a different register,
such as autodisable, invert, rshift, or sign_bit. This may have side
effects on the ASoC subsystem, which uses most of these fields for a
special purpose.

Reserving bit 0 in the shift attribute would have allowed the MSM8916
patch to be applied without additional fixups, but would also make it
difficult to add path types to the shift attribute, at best requiring
the use of macros for constructing the shift value.

Reserve bit 15 in the shift attribute to indicate the path as RX/TX.
This would reserve path types 32768-65535 when support for additional
path types is introduced.

Signed-off-by: Richard Acayan <mailingradian@gmail.com>
---
 sound/soc/qcom/qdsp6/q6voice-dai.c | 14 +++++++-------
 sound/soc/qcom/qdsp6/q6voice.h     |  6 ++++++
 2 files changed, 13 insertions(+), 7 deletions(-)

diff --git a/sound/soc/qcom/qdsp6/q6voice-dai.c b/sound/soc/qcom/qdsp6/q6voice-dai.c
index f3ae9d7..e16512d 100644
--- a/sound/soc/qcom/qdsp6/q6voice-dai.c
+++ b/sound/soc/qcom/qdsp6/q6voice-dai.c
@@ -84,7 +84,7 @@ static int q6voice_get_mixer(struct snd_kcontrol *kcontrol, struct snd_ctl_elem_
 	struct soc_mixer_control *mc =
 		(struct soc_mixer_control *)kcontrol->private_value;
 	struct q6voice *v = snd_soc_component_get_drvdata(c);
-	bool capture = !!mc->shift;
+	bool capture = !!(mc->shift & Q6VOICE_CAPTURE);
 
 	ucontrol->value.integer.value[0] =
 		q6voice_get_port(v, Q6VOICE_PATH_VOICE, capture) == mc->reg;
@@ -99,7 +99,7 @@ static int q6voice_put_mixer(struct snd_kcontrol *kcontrol, struct snd_ctl_elem_
 		(struct soc_mixer_control *)kcontrol->private_value;
 	struct q6voice *v = snd_soc_component_get_drvdata(c);
 	bool val = !!ucontrol->value.integer.value[0];
-	bool capture = !!mc->shift;
+	bool capture = !!(mc->shift & Q6VOICE_CAPTURE);
 
 	if (val)
 		q6voice_set_port(v, Q6VOICE_PATH_VOICE, capture, mc->reg);
@@ -111,15 +111,15 @@ static int q6voice_put_mixer(struct snd_kcontrol *kcontrol, struct snd_ctl_elem_
 }
 
 static const struct snd_kcontrol_new voice_tx_mixer_controls[] = {
-	SOC_SINGLE_EXT("PRI_MI2S_TX", PRIMARY_MI2S_TX, 1, 1, 0,
+	SOC_SINGLE_EXT("PRI_MI2S_TX", PRIMARY_MI2S_TX, Q6VOICE_CAPTURE, 1, 0,
 		       q6voice_get_mixer, q6voice_put_mixer),
-	SOC_SINGLE_EXT("SEC_MI2S_TX", SECONDARY_MI2S_TX, 1, 1, 0,
+	SOC_SINGLE_EXT("SEC_MI2S_TX", SECONDARY_MI2S_TX, Q6VOICE_CAPTURE, 1, 0,
 		       q6voice_get_mixer, q6voice_put_mixer),
-	SOC_SINGLE_EXT("TERT_MI2S_TX", TERTIARY_MI2S_TX, 1, 1, 0,
+	SOC_SINGLE_EXT("TERT_MI2S_TX", TERTIARY_MI2S_TX, Q6VOICE_CAPTURE, 1, 0,
 		       q6voice_get_mixer, q6voice_put_mixer),
-	SOC_SINGLE_EXT("QUAT_MI2S_TX", QUATERNARY_MI2S_TX, 1, 1, 0,
+	SOC_SINGLE_EXT("QUAT_MI2S_TX", QUATERNARY_MI2S_TX, Q6VOICE_CAPTURE, 1, 0,
 		       q6voice_get_mixer, q6voice_put_mixer),
-	SOC_SINGLE_EXT("QUIN_MI2S_TX", QUINARY_MI2S_TX, 1, 1, 0,
+	SOC_SINGLE_EXT("QUIN_MI2S_TX", QUINARY_MI2S_TX, Q6VOICE_CAPTURE, 1, 0,
 		       q6voice_get_mixer, q6voice_put_mixer),
 };
 
diff --git a/sound/soc/qcom/qdsp6/q6voice.h b/sound/soc/qcom/qdsp6/q6voice.h
index 5b2d2dd..ec876fa 100644
--- a/sound/soc/qcom/qdsp6/q6voice.h
+++ b/sound/soc/qcom/qdsp6/q6voice.h
@@ -2,6 +2,12 @@
 #ifndef _Q6_VOICE_H
 #define _Q6_VOICE_H
 
+#define Q6VOICE_CAPTURE BIT(15)
+
+/*
+ * Path types from 0x8000 - 0xffff and similar sequences occurring every 65536
+ * values are reserved for specifying whether the path is capture or playback.
+ */
 enum q6voice_path_type {
 	Q6VOICE_PATH_VOICE	= 0,
 	/* TODO: Q6VOICE_PATH_VOIP	= 1, */
