From: Dmitry Baryshkov <dmitry.baryshkov@linaro.org>
Date: Thu, 4 Jul 2024 14:12:46 +0300
Subject: firmware: qcom: tzmem: blacklist more platforms for SHM Bridge

The SHM bridge makes the Qualcomm RB3 and SM8150-HDK reset while probing
the RMTFS (in qcom_scm_assign_mem()). Blacklist the SHM Bridge on
corresponding platforms using SoC-level compat string. If later it's
found that the bad behaviour is limited just to the particular boards
rather than SoC, the compat strings can be adjusted.

Reported-by: Neil Armstrong <neil.armstrong@linaro.org>
Fixes: f86c61498a57 ("firmware: qcom: tzmem: enable SHM Bridge support")
Signed-off-by: Dmitry Baryshkov <dmitry.baryshkov@linaro.org>
Acked-by: Bartosz Golaszewski <bartosz.golaszewski@linaro.org>
Tested-by: Neil Armstrong <neil.armstrong@linaro.org> # on Qualcomm RB3
Link: https://lore.kernel.org/r/20240704-shmbridge-blacklist-v1-1-14b027b3b2dc@linaro.org
Signed-off-by: Bjorn Andersson <andersson@kernel.org>
(cherry picked from commit 55751d3e9e96d5d64dc6ebb5bbdf70f45098f279)
Parent: firmware: qcom: tzmem: simplify returning pointer without cleanup
Signed-off-by: Richard Acayan <mailingradian@gmail.com>
---
 drivers/firmware/qcom/qcom_tzmem.c | 2 ++
 1 file changed, 2 insertions(+)

diff --git a/drivers/firmware/qcom/qcom_tzmem.c b/drivers/firmware/qcom/qcom_tzmem.c
index ab156ab..17948cfc 100644
--- a/drivers/firmware/qcom/qcom_tzmem.c
+++ b/drivers/firmware/qcom/qcom_tzmem.c
@@ -78,6 +78,8 @@ static bool qcom_tzmem_using_shm_bridge;
 /* List of machines that are known to not support SHM bridge correctly. */
 static const char *const qcom_tzmem_blacklist[] = {
 	"qcom,sc8180x",
+	"qcom,sdm845", /* reset in rmtfs memory assignment */
+	"qcom,sm8150", /* reset in rmtfs memory assignment */
 	NULL
 };
 
