From: Richard Acayan <mailingradian@gmail.com>
Date: Tue, 25 Jun 2024 19:44:05 -0400
Subject: arm64: dts: qcom: sdm670-google: add rt5514 microphone codec

The Pixel 3a/3a XL have a Realtek ALC5513 codec for its built-in
microphone (compatible with ALC5514 drivers). Add the ALC5513 codec, the
Q6AFE TDM port, and Q6ASM MultiMedia4 to support audio capture.

Signed-off-by: Richard Acayan <mailingradian@gmail.com>
---
 arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi | 130 ++++++++++++++++++++-
 1 file changed, 129 insertions(+), 1 deletion(-)

diff --git a/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi b/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
index 57eeb65..f17f41d 100644
--- a/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
+++ b/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
@@ -196,13 +196,15 @@ sound {
 		pinctrl-names = "default";
 		pinctrl-0 = <&cdc_pdm_default>,
 			    <&cdc_dmic_default>,
+			    <&pri_tdm_default>,
 			    <&sec_tdm_default>;
 
 		audio-routing = "Analog PDM_RX1", "Digital PDM_RX1",
 				"Analog PDM_RX2", "Digital PDM_RX2",
 				"Analog PDM_RX3", "Digital PDM_RX3",
 				"Digital LPASS_PDM_TX", "Analog PDM_TX",
-				"Analog AMIC2", "Analog MIC BIAS External2";
+				"Analog AMIC2", "Analog MIC BIAS External2",
+				"Stereo1 DMIC Mux", "Analog MIC BIAS Internal1";
 
 		mm1-dai-link {
 			link-name = "MultiMedia1";
@@ -228,6 +230,14 @@ cpu {
 			};
 		};
 
+		mm4-dai-link {
+			link-name = "MultiMedia4";
+
+			cpu {
+				sound-dai = <&q6asmdai MSM_FRONTEND_DAI_MULTIMEDIA4>;
+			};
+		};
+
 		voicemmode1-dai-link {
 			link-name = "VoiceMMode1";
 
@@ -236,6 +246,22 @@ cpu {
 			};
 		};
 
+		pri-tdm-tx-link {
+			link-name = "Primary TDM0 Capture";
+
+			cpu {
+				sound-dai = <&q6afedai PRIMARY_TDM_TX_0>;
+			};
+
+			platform {
+				sound-dai = <&q6routing>;
+			};
+
+			codec {
+				sound-dai = <&rt5514_i2c 0>;
+			};
+		};
+
 		sec-tdm-rx-link {
 			link-name = "Secondary TDM0 Playback";
 
@@ -821,6 +847,12 @@ cs35l36_bottom: audio-codec@41 {
 		#sound-dai-cells = <1>;
 	};
 
+	rt5514_i2c: audio-codec@57 {
+		compatible = "realtek,rt5514";
+		reg = <0x57>;
+		#sound-dai-cells = <1>;
+	};
+
 	drv2624: haptics@5a {
 		compatible = "ti,drv2624";
 		reg = <0x5a>;
@@ -984,6 +1016,7 @@ &pm660l_codec {
 	vdd-micbias-supply = <&vreg_l7b_3p125>;
 
 	qcom,micbias-lvl = <2500>;
+	qcom,micbias1-ext-cap;
 
 	status = "okay";
 };
@@ -1036,6 +1069,17 @@ &qupv3_id_1 {
 };
 
 &q6afedai {
+	dai@19 {
+		reg = <PRIMARY_TDM_TX_0>;
+
+		qcom,tdm-sync-mode = <0>;
+		qcom,tdm-sync-src = <1>;
+		qcom,tdm-data-out = <0>;
+		qcom,tdm-invert-sync = <0>;
+		qcom,tdm-data-delay = <1>;
+		qcom,tdm-data-align = <0>;
+	};
+
 	dai@28 {
 		reg = <SECONDARY_TDM_RX_0>;
 
@@ -1070,6 +1114,10 @@ dai@1 {
 	dai@2 {
 		reg = <2>;
 	};
+
+	dai@3 {
+		reg = <3>;
+	};
 };
 
 &q6voicedai {
@@ -1092,6 +1140,22 @@ &sdhc_1 {
 	status = "okay";
 };
 
+&spi2 {
+	status = "okay";
+
+	rt5514_spi: audio-codec@0 {
+		compatible = "realtek,rt5514";
+		reg = <0x00>;
+		interrupts-extended = <&tlmm 126 IRQ_TYPE_EDGE_RISING>;
+		pinctrl-names = "default";
+		pinctrl-0 = <&rt5514_spi_default>;
+		spi-max-frequency = <10000000>;
+		spi-cpol;
+		spi-cpha;
+		#sound-dai-cells = <1>;
+	};
+};
+
 &tlmm {
 	gpio-reserved-ranges = <0 4>, <81 4>;
 
@@ -1168,6 +1232,70 @@ mode-pins {
 		};
 	};
 
+	pri_tdm_default: pri-tdm-default-state {
+		misc-pins {
+			pins = "gpio65";
+			function = "pri_mi2s";
+			drive-strength = <8>;
+		};
+
+		ws-pins {
+			pins = "gpio66";
+			function = "pri_mi2s_ws";
+			drive-strength = <8>;
+		};
+
+		dout-pins {
+			pins = "gpio67";
+			function = "pri_mi2s";
+			drive-strength = <8>;
+		};
+
+		din-pins {
+			pins = "gpio68";
+			function = "pri_mi2s";
+			drive-strength = <8>;
+		};
+	};
+
+	pri_tdm_sleep: pri-tdm-sleep-state {
+		misc-pins {
+			pins = "gpio65";
+			function = "pri_mi2s";
+			drive-strength = <2>;
+			bias-pull-down;
+		};
+
+		ws-pins {
+			pins = "gpio66";
+			function = "pri_mi2s_ws";
+			drive-strength = <2>;
+			bias-pull-down;
+		};
+
+		dout-pins {
+			pins = "gpio67";
+			function = "pri_mi2s";
+			drive-strength = <2>;
+			bias-pull-down;
+		};
+
+		din-pins {
+			pins = "gpio68";
+			function = "pri_mi2s";
+			drive-strength = <2>;
+			bias-pull-down;
+		};
+	};
+
+	rt5514_spi_default: rt5514-spi-default-state {
+		pins = "gpio126";
+		function = "gpio";
+		drive-strength = <2>;
+		bias-pull-down;
+		output-disable;
+	};
+
 	touchscreen_default: ts-default-state {
 		ts-reset-pins {
 			pins = "gpio99";
