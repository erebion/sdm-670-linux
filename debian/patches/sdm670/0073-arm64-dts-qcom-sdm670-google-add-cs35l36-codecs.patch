From: Richard Acayan <mailingradian@gmail.com>
Date: Thu, 16 Mar 2023 22:46:13 -0400
Subject: arm64: dts: qcom: sdm670-google: add cs35l36 codecs

The Pixel 3a has CS35L36 for playback at the earpiece and bottom
speaker. There are also 8 capture channels, the locating of which is
left as a challenge for anyone with this device reading the commit logs.

This does not introduce audio capture support.

Signed-off-by: Richard Acayan <mailingradian@gmail.com>
---
 arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi | 149 ++++++++++++++++++++-
 1 file changed, 148 insertions(+), 1 deletion(-)

diff --git a/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi b/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
index 4691e2f..76c4201 100644
--- a/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
+++ b/arch/arm64/boot/dts/qcom/sdm670-google-common.dtsi
@@ -11,6 +11,7 @@
 #include <dt-bindings/input/input.h>
 #include <dt-bindings/pinctrl/qcom,pmic-gpio.h>
 #include <dt-bindings/power/qcom-rpmpd.h>
+#include <dt-bindings/sound/qcom,q6afe.h>
 #include <dt-bindings/sound/qcom,q6asm.h>
 #include "sdm670.dtsi"
 #include "pm660.dtsi"
@@ -184,7 +185,8 @@ sound {
 		model = "Google Pixel 3a";
 
 		pinctrl-names = "default";
-		pinctrl-0 = <&cdc_pdm_default>;
+		pinctrl-0 = <&cdc_pdm_default>,
+			    <&sec_tdm_default>;
 
 		mm1-dai-link {
 			link-name = "MultiMedia1";
@@ -209,6 +211,23 @@ cpu {
 				sound-dai = <&q6asmdai MSM_FRONTEND_DAI_MULTIMEDIA3>;
 			};
 		};
+
+		sec-tdm-rx-link {
+			link-name = "Secondary TDM0 Playback";
+
+			cpu {
+				sound-dai = <&q6afedai SECONDARY_TDM_RX_0>;
+			};
+
+			platform {
+				sound-dai = <&q6routing>;
+			};
+
+			codec {
+				sound-dai = <&cs35l36_top 0>,
+					    <&cs35l36_bottom 0>;
+			};
+		};
 	};
 
 	/*
@@ -560,6 +579,62 @@ &i2c10 {
 	clock-frequency = <400000>;
 	status = "okay";
 
+	cs35l36_top: audio-codec@40 {
+		/* top speaker - portrait left */
+		compatible = "cirrus,cs35l36";
+		reg = <0x40>;
+
+		cirrus,boost-ind-nanohenry = <1000>;
+		cirrus,boost-ctl-select = <0x01>;
+		cirrus,boost-ctl-millivolt = <10000>;
+		cirrus,boost-peak-milliamp = <2600>;
+		cirrus,weak-fet-delay = <0x04>;
+		cirrus,weak-fet-thld = <0x01>;
+		cirrus,temp-warn-threshold = <0x01>;
+		cirrus,multi-amp-mode;
+
+		cirrus,irq-drive-select = <0x01>;
+		cirrus,irq-gpio-select = <0x00>;
+
+		sound-name-prefix = "Top";
+
+		interrupts-extended = <&tlmm 80 IRQ_TYPE_LEVEL_HIGH>;
+
+		reset-gpios = <&tlmm 14 GPIO_ACTIVE_HIGH>;
+
+		pinctrl-names = "default";
+		pinctrl-0 = <&cs35l36_top_default>;
+
+		#sound-dai-cells = <1>;
+	};
+
+	cs35l36_bottom: audio-codec@41 {
+		/* bottom speaker - portrait right */
+		compatible = "cirrus,cs35l36";
+		reg = <0x41>;
+
+		cirrus,boost-ind-nanohenry = <1000>;
+		cirrus,boost-ctl-select = <0x01>;
+		cirrus,boost-ctl-millivolt = <10000>;
+		cirrus,boost-peak-milliamp = <2600>;
+		cirrus,temp-warn-threshold = <0x01>;
+		cirrus,multi-amp-mode;
+
+		cirrus,irq-drive-select = <0x01>;
+		cirrus,irq-gpio-select = <0x00>;
+
+		sound-name-prefix = "Bottom";
+
+		interrupts-extended = <&tlmm 79 IRQ_TYPE_LEVEL_HIGH>;
+
+		reset-gpios = <&tlmm 16 GPIO_ACTIVE_HIGH>;
+
+		pinctrl-names = "default";
+		pinctrl-0 = <&cs35l36_bottom_default>;
+
+		#sound-dai-cells = <1>;
+	};
+
 	drv2624: haptics@5a {
 		compatible = "ti,drv2624";
 		reg = <0x5a>;
@@ -591,6 +666,31 @@ &ipa {
 	status = "okay";
 };
 
+&lpi_tlmm {
+	sec_tdm_default: sec-tdm-default-state {
+		misc-pins {
+			pins = "gpio8", "gpio9";
+			function = "sec_tdm";
+			drive-strength = <8>;
+			bias-disable;
+		};
+
+		din-pins {
+			pins = "gpio10";
+			function = "sec_tdm_din";
+			drive-strength = <8>;
+			bias-disable;
+		};
+
+		dout-pins {
+			pins = "gpio11";
+			function = "sec_tdm_dout";
+			drive-strength = <8>;
+			bias-disable;
+		};
+	};
+};
+
 &mdss {
 	status = "okay";
 };
@@ -681,6 +781,19 @@ &qupv3_id_1 {
 	status = "okay";
 };
 
+&q6afedai {
+	dai@28 {
+		reg = <SECONDARY_TDM_RX_0>;
+
+		qcom,tdm-sync-mode = <0>;
+		qcom,tdm-sync-src = <1>;
+		qcom,tdm-data-out = <0>;
+		qcom,tdm-invert-sync = <0>;
+		qcom,tdm-data-delay = <1>;
+		qcom,tdm-data-align = <0>;
+	};
+};
+
 &q6asmdai {
 	dai@0 {
 		reg = <0>;
@@ -712,6 +825,40 @@ &sdhc_1 {
 &tlmm {
 	gpio-reserved-ranges = <0 4>, <81 4>;
 
+	cs35l36_top_default: cs35l36-top-default-state {
+		reset-pins {
+			pins = "gpio14";
+			function = "gpio";
+			drive-strength = <2>;
+			output-high;
+		};
+
+		irq-pins {
+			pins = "gpio80";
+			function = "gpio";
+			drive-strength = <2>;
+			bias-pull-down;
+			output-disable;
+		};
+	};
+
+	cs35l36_bottom_default: cs35l36-bottom-default-state {
+		reset-pins {
+			pins = "gpio16";
+			function = "gpio";
+			drive-strength = <2>;
+			output-high;
+		};
+
+		irq-pins {
+			pins = "gpio79";
+			function = "gpio";
+			drive-strength = <2>;
+			bias-pull-down;
+			output-disable;
+		};
+	};
+
 	panel_default: panel-default-state {
 		te-pins {
 			pins = "gpio10";
